import 'dart:convert';

Object objectFromMap(String str) => Object.fromMap(json.decode(str));

String objectToMap(Object data) => json.encode(data.toMap());

class Object {
  Object({
    this.brakeType,
    this.type,
    this.frameMaterial,
    this.ridingStyle,
    this.models,
    this.brands,
    this.flavors,
    this.nutritionBrands,
    this.nutritionModels,
  });

  List<BrakeType> brakeType;
  List<BrakeType> type;
  List<BrakeType> frameMaterial;
  List<BrakeType> ridingStyle;
  List<BrakeType> models;
  List<BrakeType> brands;
  List<BrakeType> flavors;
  List<BrakeType> nutritionBrands;
  List<BrakeType> nutritionModels;


  factory Object.fromMap(Map<String, dynamic> json) => Object(
        brakeType: List<BrakeType>.from(
            json["BrakeType"].map((x) => BrakeType.fromMap(x))),
        type:
            List<BrakeType>.from(json["Type"].map((x) => BrakeType.fromMap(x))),
        frameMaterial: List<BrakeType>.from(
            json["FrameMaterial"].map((x) => BrakeType.fromMap(x))),
        ridingStyle: List<BrakeType>.from(
            json["RidingStyle"].map((x) => BrakeType.fromMap(x))),
        models: List<BrakeType>.from(
            json["Models"].map((x) => BrakeType.fromMap(x))),
        brands: List<BrakeType>.from(
            json["Brands"].map((x) => BrakeType.fromMap(x))),
        flavors: List<BrakeType>.from(
            json["Flavors"].map((x) => BrakeType.fromMap(x))),
        nutritionBrands: List<BrakeType>.from(
            json["NutritionBrands"].map((x) => BrakeType.fromMap(x))),
        nutritionModels: List<BrakeType>.from(
            json["NutritionModels"].map((x) => BrakeType.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "BrakeType": List<dynamic>.from(brakeType.map((x) => x.toMap())),
        "Type": List<dynamic>.from(type.map((x) => x.toMap())),
        "FrameMaterial":
            List<dynamic>.from(frameMaterial.map((x) => x.toMap())),
        "RidingStyle": List<dynamic>.from(ridingStyle.map((x) => x.toMap())),
        "Models": List<dynamic>.from(models.map((x) => x.toMap())),
        "Brands": List<dynamic>.from(brands.map((x) => x.toMap())),
        "Flavors": List<dynamic>.from(flavors.map((x) => x.toMap())),
        "NutritionBrands":
            List<dynamic>.from(nutritionBrands.map((x) => x.toMap())),
        "NutritionModels":
            List<dynamic>.from(nutritionModels.map((x) => x.toMap())),
      };
}

class BrakeType {
  BrakeType({
    this.id,
    this.title,
    this.createdAt,
    this.updatedAt,
  });
 
  int id;
  String title;
  dynamic createdAt;
  dynamic updatedAt;

  factory BrakeType.fromMap(Map<String, dynamic> json) => BrakeType(
        id: json["id"],
        title: json["title"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "title": title,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };
}
