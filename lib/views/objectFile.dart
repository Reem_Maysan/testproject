import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'objectsList.dart';
import 'package:test1/models/object.dart';

class MyList extends StatelessWidget {
  final List<BrakeType> elementList;
  MyList(this.elementList);

  List<Widget> _getChildren() {
    List<Widget> children = [];
    elementList.forEach((element) {
      children.add(
        new MyExpansionTile(element.title, element.id),
      );
    });
    return children;
  }

  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: _getChildren(),
    );
  }
}

class MyExpansionTile extends StatefulWidget {
  String title;
  int id;
  MyExpansionTile(this.title, this.id);

  @override
  _MyExpansionTileState createState() => _MyExpansionTileState();
}

class _MyExpansionTileState extends State<MyExpansionTile> {
  PageStorageKey _key;

  @override
  Widget build(BuildContext context) {
    _key = new PageStorageKey('${widget.id}');
    return new ExpansionTile(
        key: _key,
        title: new Text(widget.title),
        children: <Widget>[
          FutureBuilder(
              future: fetchBrakeType(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: const Text('Loading...'),
                  );
                } else {
                  List<Widget> reasonList = [];
                  List<BrakeType> brakeType = snapshot.data;
                  brakeType.forEach((f) {
                    reasonList.add(ListTile(
                      dense: true,
                      title: new Text(f.title),
                    ));
                  });
                  return new Column(children: reasonList);
                }
              }),
        ]);
  }
}

class ObjectFile extends StatefulWidget {
  @override
  _ObjectFileState createState() => _ObjectFileState();
}

class _ObjectFileState extends State<ObjectFile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Objects'),
        ),
        body: Container(
          child: FutureBuilder(
              future: fetchBrakeType(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: const Text('Loading...'),
                  );
                } else
                  return MyList(snapshot.data);
              }),
        ));
  }
}
